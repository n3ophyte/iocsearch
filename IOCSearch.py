"""
IOC ElasticSearch Hunting
-------------------------

 The MIT License

 Copyright 2015 @n3ophyte

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

-------------------------
"""
import json
import os, sys, traceback
from optparse import OptionParser
import ConfigParser
import time
import multiprocessing
import threading
from Queue import Queue, Empty

# How Many Parallel Workers running parallel IOC
num_worker_threads = 8

USAGE = """ -v for version -d for debug -t for troubleshoot IOC time"""
#Command line options
VERSION = "v0.1 IOC Cyber Threat Intelligence"

parser = OptionParser(USAGE)

parser.add_option("-v",
                  action="store_true", dest="version",
                  default=False,
                  help="show version")
parser.add_option("-d",
                  action="store_true", dest="debug",
                  default=False,
                  help="show debug information")
parser.add_option("-t",
                  action="store_true", dest="testing",
                  default=False,
                  help="Test all IOC for Performances")
(options, args) = parser.parse_args()


# full path of installation dir
global_configuration = 'global.cfg'
SECTIONS = ['INPUT', 'RULES', 'OUTPUT' ]  #Keep ordering
test_ioc = False

if options.testing:
    if len(args) < 1:
        print "Testing option need name of IOC"
        sys.exit(0)
    else:
        test_ioc = True
        ioc_to_test = args[0]
        if options.debug:
            print "Testing IOC: "+ioc_to_test


def PrintException():
    """


    """
    for frame in traceback.extract_tb(sys.exc_info()[2]):
        fname,lineno,fn,text = frame
        print "Error in %s on line %d" % (fname, lineno)


# Check if this IOC has to be run from its frequency time
# return frequency from IOC file if IOC time is evaluated otherwise return False
def caching(ioc_name, frequency, lock):
    """

    :param ioc_name:
    :param frequency:
    :param lock:
    :return:
    """
    lock.acquire()
    cache_file = "./cache.json"
    global test_ioc
    # If this is a test we want to save cache!
    if test_ioc:
        return {'from':int(str(time.time()).split('.')[0]) - int(frequency[:-1]) , 'to': int(str(time.time()).split('.')[0])}
    if os.path.exists(cache_file):
        if options.debug:
            print 'load cache file'
        file_cache_stats = os.stat(cache_file)
        # purge cache file if it is too big
        # TODO: we should clean old email instead of remove all the cache file
        if int(file_cache_stats.st_size) > (1024*1024*10):
            try:
                os.remove(cache_file)
            except OSError:
                pass
            lock.release()
            # UTC conversion and from / to range for ES query
            return {'from':int(str(time.time()).split('.')[0]) - int(frequency[:-1]),'to':int(str(time.time()).split('.')[0])}
        data = json.load(open(cache_file))
        if ioc_name in data:
            # IOC has to be RUN! frequency elasped
            if int(str(time.time()).split('.')[0]) - data[ioc_name]['last_execution'] >= int(frequency[:-1]):
                if options.debug:
                    print "Run IOC "+ioc_name+" last_execution: "+str(data[ioc_name]['last_execution'])+' time: '+str(time.time()).split('.')[0]+' freq:'+frequency[:-1]
                # TODO: is this var thread-safe ?
                last_execution = data[ioc_name]['last_execution']
                data[ioc_name]['last_execution'] = int(str(time.time()).split('.')[0])
                json.dump(data, open(cache_file, 'w'))
                # We return last execution time so we can select logs that we missed due to overload or time diff
                # plus UTC conversion for ELK @timestamp range
                lock.release()
                return {'from': int(last_execution), 'to': int(str(time.time()).split('.')[0])}
            else:
                # IOC has already by ran its time is inside frequency
                if options.debug:
                    print "IOC Already ran "+ioc_name+" last_execution: "+str(data[ioc_name]['last_execution'])+' time: '+str(time.time()).split('.')[0]+' freq:'+frequency[:-1]
                lock.release()
                return False
        else:
            # First Time that IOC Run
            data[ioc_name] = {}
            data[ioc_name]['last_execution'] = int(str(time.time()).split('.')[0])
            json.dump(data, open(cache_file, 'w'))
            if options.debug:
                print "First Time IOC Run "+ioc_name
            lock.release()
            return {'from':int(str(time.time()).split('.')[0]) - int(frequency[:-1]) , 'to': int(str(time.time()).split('.')[0])}
    else:
        # cache file doesnt exist
        data = {ioc_name: {"last_execution": int(str(time.time()).split('.')[0])}}
        json.dump(data, open(cache_file, 'w'))
        if options.debug:
            print "Creating cache file with IOC: "+ioc_name
        lock.release()
        return {'from':int(str(time.time()).split('.')[0]) - int(frequency[:-1]),'to':int(str(time.time()).split('.')[0])}


def worker(localdata,lock,q):

    """

    :param localdata: local var for every thread that will contains IOC config options (INPUT RULES OUTPUT)
    :param lock: lock for queue disk write or disk cache json file
    :param q: QUEUE of the all IOC that Threat will get and run
    """
    localdata.CONFIG = {}
    localdata.CONFIG['INPUT'] = {}
    localdata.CONFIG['RULES'] = {}
    localdata.CONFIG['OUTPUT'] = {}
    while True:
        ioc_path = './ioc/'
        try:
            ioc = q.get_nowait()
        except Empty:
            break
        # Sentinel Value end of Queue
        if ioc == None:
            break
        try:
            if os.path.isfile(ioc_path+ioc):
                if os.access(ioc_path+ioc, os.R_OK):
                    #Config parser
                    localdata.Config = ConfigParser.ConfigParser()
                    localdata.Config.read(ioc_path+ioc)
                    for section in localdata.Config.sections():
                        if options.debug:
                            print "Parsing "+ioc, section
                        if section not in SECTIONS:
                            continue
                        # Special handling for GLOBAL
                        localdata.CONFIG[section] = {}
                        for localdata.option, localdata.value in localdata.Config.items(section):
                            if options.debug:
                                print "    %s = %s" % (localdata.option, localdata.value)
                            localdata.CONFIG[section][localdata.option] = localdata.value
                if localdata.CONFIG['INPUT']['status'] == "disabled":
                    continue
                # RUN IOC
                if localdata.CONFIG['INPUT']['connection'] == "elasticsearch":
                    # What type of IOC is this?
                    # list = file with something to match in every line
                    # simple_query_chain = run query extract every field and for every field run another query
                    # simple_query = read query from disk and run as it is
                    if localdata.CONFIG['INPUT']['rule-type'] == "list":
                        import modules.es_list as el
                        # We take the last time that IOC run so we avoid some time diff between execution of many IOC
                        localdata.overwrite_frequency = caching(ioc, localdata.CONFIG['INPUT']['frequency'], lock)
                        localdata.iocdata = {}
                        localdata.iocdata[ioc] = {}
                        localdata.iocdata[ioc]['input'] = localdata.CONFIG['INPUT']
                        localdata.iocdata[ioc]['rules'] = localdata.CONFIG['RULES']
                        localdata.iocdata[ioc]['output'] = localdata.CONFIG['OUTPUT']
                        localdata.iocdata[ioc]['overwrite_frequency'] = localdata.overwrite_frequency
                        el.run_list(localdata.iocdata[ioc],options)
                    if localdata.CONFIG['INPUT']['rule-type'] == "simple_query_chain":
                        import modules.es_query_chain as ec
                        localdata.overwrite_frequency = caching(ioc, localdata.CONFIG['INPUT']['frequency'], lock)
                        if options.debug:
                            print "Overwride Frequency: "+str(localdata.overwrite_frequency)
                        localdata.iocdata = {}
                        localdata.iocdata[ioc] = {}
                        localdata.iocdata[ioc]['input'] = localdata.CONFIG['INPUT']
                        localdata.iocdata[ioc]['rules'] = localdata.CONFIG['RULES']
                        localdata.iocdata[ioc]['output'] = localdata.CONFIG['OUTPUT']
                        localdata.iocdata[ioc]['overwrite_frequency'] = localdata.overwrite_frequency
                        ec.es_query_chain(localdata.iocdata[ioc], options)
                    if localdata.CONFIG['INPUT']['rule-type'] == "simple_query":
                        import modules.es_simple_query as eq
                        localdata.overwrite_frequency = caching(ioc, localdata.CONFIG['INPUT']['frequency'], lock)
                        localdata.iocdata = {}
                        localdata.iocdata[ioc] = {}
                        localdata.iocdata[ioc]['input'] = localdata.CONFIG['INPUT']
                        localdata.iocdata[ioc]['rules'] = localdata.CONFIG['RULES']
                        localdata.iocdata[ioc]['output'] = localdata.CONFIG['OUTPUT']
                        localdata.iocdata[ioc]['overwrite_frequency'] = localdata.overwrite_frequency
                        eq.simple_query(localdata.iocdata[ioc], options)
                else:
                    print "Unable to load configuration file:", options.config
                    print "Using default configuration"
        except Exception as inst:
            #q.task_done()
            print inst
            PrintException()
            break
        #q.task_done()

# Queue Thread Dispatcher
q = multiprocessing.Manager().Queue()
if test_ioc:
    q.put(ioc_to_test)
else:
    for ioc in os.listdir('./ioc'):
        q.put(ioc)
# Sentinel
q.put(None)
# Spawn Thread base on max thread and how many IOC then try to join all the results
localdata = threading.local()
processes = []
# Thread Lock for Cache file!!!
lock = multiprocessing.Lock()

for i in range(num_worker_threads):
    t = multiprocessing.Process(target=worker, args=(localdata,lock,q))
    t.daemon = True
    processes.append(t)
    t.start()
    #print threading.activeCount()

for t in processes:
    t.join()

exit(0)




