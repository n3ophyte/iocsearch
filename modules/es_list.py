__author__ = 'stefano'
import rules
import datetime
import time
from elasticsearch import Elasticsearch


# Return False we need to skip output and of course this IOC
def run_list(iocdata,options):
    """

    :param iocdata:
    :param options:
    :return:
    """
    if not iocdata['overwrite_frequency']:
        return False
    if options.debug:
        print "from frequency:" + str(iocdata['overwrite_frequency']['from'])
        print "to frequency: " + str(iocdata['overwrite_frequency']['to'])
    if iocdata['input']['username'] and iocdata['input']['password']:
        # ,use_ssl=True
        es = Elasticsearch(host=iocdata['input']['host'],
                             port=iocdata['input']['port'], http_auth=(iocdata['input']['username'], iocdata['input']['password']),use_ssl=iocdata['input']['use_ssl'])
    else:
        es = Elasticsearch(host=iocdata['input']['host'], port=iocdata['input']['port'], use_ssl=iocdata['input']['use_ssl'])
    # yesterday index
    yesterday = datetime.datetime.utcfromtimestamp(time.time()) - datetime.timedelta(days=1)
    yesterday_index = yesterday.strftime("%Y.%m.%d")
    with open(iocdata['input']['source']) as ins:
        for ip in ins:
            ip = ip.rstrip('\n')
            ip = ip.rstrip('\r')
            try:
                res = es.search(
                    index=[iocdata['input']['index_prefix'] +  time.strftime("%Y.%m.%d", time.gmtime()), iocdata['input']['index_prefix'] + yesterday_index],
                    doc_type=iocdata['input']['type'],
                    body="""{ \"query\": {
                    \"filtered\": {
                      \"query\": {
                        \"bool\": {
                          \"should\": [
                            {
                              \"query_string\": {
                                \"query\": \""""+ip+""" AND NOT icmp\"
                              }
                            }
                          ]
                        }
                      },
                    \"filter\": {
                    \"bool\" : {
                      \"must\": [ {
                          \"range\": {
                            \"@timestamp\" : {
                              \"from\":\"""" + str(iocdata['overwrite_frequency']['from']*1000) + """\",
                              \"to\":\"""" + str(iocdata['overwrite_frequency']['to']*1000) + """\" }}}]}}}}}""")
            except Exception as inst:
                print inst
                break

            if rules.rule_parsing(iocdata['rules'],iocdata['output'],res, options, es):
                return True

            #for event in res['hits']['hits']:
            #    print res['hits']['total']
            #    if "_source" in event:
            #        if "dst_port" in event['_source']:
            #            print "src ip: "+event['_source']['src_ip']+" dst ip: "+event['_source']['dst_ip']+" service: "+event['_source']['dst_port']
                    #print "src ip: "+event['_source']['src_ip']+" dst ip: "+event['_source']['dst_ip']+" service: "+event['_source']['message']
            #    else:
            #        print res['hits']['hits']
        return False