__author__ = 'stefano'
import rules
import datetime
import time
import os
from elasticsearch import Elasticsearch


# Return False we need to skip output and of course this IOC
def es_query_chain(iocdata ,options):
    """

    :param iocdata:
    :param options:
    :return:
    """
    if iocdata['overwrite_frequency'] == False:
        return False

    if options.debug:
        print "from frequency:" + str(iocdata['overwrite_frequency']['from'])
        print "to frequency: " + str(iocdata['overwrite_frequency']['to'])
    if iocdata['input']['username'] and iocdata['input']['password']:
        # ,use_ssl=True
        es = Elasticsearch(host=iocdata['input']['host'],
                             port=iocdata['input']['port'], http_auth=(iocdata['input']['username'], iocdata['input']['password']),use_ssl=iocdata['input']['use_ssl'])
    else:
        es = Elasticsearch(host=iocdata['input']['host'], port=iocdata['input']['port'], use_ssl=iocdata['input']['use_ssl'])
    # yesterday index
    # TODO: check if index exist before adding this into query
    yesterday = datetime.datetime.utcfromtimestamp(time.time()) - datetime.timedelta(days=1)
    yesterday_index = yesterday.strftime("%Y.%m.%d")
    if not os.path.exists(iocdata['input']['source']):
        if options.debug:
            print 'Query files ' + iocdata['input']['source'] + " not found"
        exit()
    else:
        line_hash = open(iocdata['input']['source'], 'r')
        query = ""
        for line in line_hash:
            query += line

    query_range = """{ """+query+""",
                    \"filter\": {
                    \"bool\" : {
                      \"must\": [ {
                          \"range\": {
                            \"@timestamp\" : {
                              \"from\":\"""" + str(iocdata['overwrite_frequency']['from']*1000) + """\",
                              \"to\":\"""" + str(iocdata['overwrite_frequency']['to']*1000) + """\" }}}]}}}"""
    if options.debug:
        print "Query for IOC: "+str(query_range)
    # Scan scroll query
    scanResp = es.search(index=[iocdata['input']['index_prefix'] +  time.strftime("%Y.%m.%d", time.gmtime()), iocdata['input']['index_prefix'] + yesterday_index], doc_type=iocdata['input']['type'], body=query_range,
                 search_type="scan", scroll="5m", size="1000")
    scrollID = scanResp['_scroll_id']
    while True:
        results = es.scroll(scroll_id=scrollID, scroll="5m")
        #print results
        if len(results['hits']['hits']) == 0:
            if options.debug:
                print "end of scroll"
            break
        scrollID = results['_scroll_id']
        # no match so we should break this IOC
        if int(results['hits']['total']) == 0:
            break
        for data_to_send in results['hits']['hits']:
            if rules.rule_parsing(iocdata['rules'],iocdata['output'],data_to_send, options, es):
                return True

            #for event in res['hits']['hits']:
            #    print res['hits']['total']
            #    if "_source" in event:
            #        if "dst_port" in event['_source']:
            #            print "src ip: "+event['_source']['src_ip']+" dst ip: "+event['_source']['dst_ip']+" service: "+event['_source']['dst_port']
                    #print "src ip: "+event['_source']['src_ip']+" dst ip: "+event['_source']['dst_ip']+" service: "+event['_source']['message']
            #    else:
            #        print res['hits']['hits']
    return False