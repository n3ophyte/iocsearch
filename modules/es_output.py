__author__ = 'stefano'
# Import smtplib for the actual sending function
import smtplib
import datetime
import sys
# Import the email modules we'll need
from email.mime.text import MIMEText

debug = False


def sendMail(smtp_host,subject_header,data,to,from_mail,subject):
    """

    :param smtp_host:
    :param subject_header:
    :param data:
    :param to:
    :param from_mail:
    :param subject:
    """
    if not subject:
        print "Email Subject Missing"
        sys.exit()
    msg = MIMEText(data)
    msg['Subject'] = subject_header+subject
    #msg = MIMEMultipart()
    msg['From'] = from_mail

    if ',' in to:
        receiver = to.split(',')
        msg['To'] = ', '.join( receiver )
    else:
        msg['To'] = to
        receiver = to

        if debug:
            print msg.as_string()
        s = smtplib.SMTP(smtp_host)
        s.sendmail(from_mail, receiver, msg.as_string())


# CONFIG[OUTPUT] what shall we do if something match?
def process_output(output,message,total,risk,elastic_object):
    """

    :param output: output configuration of IOC
    :param message: array of match that we loop in the email body
    :param total: total match of the IOC
    :param risk: risk of the IOC
    :param elastic_object: elasticsearch instance/object so we can run query if needed
    :return: ???
    """
    global debug
    debug = output['debug']
    if not message:
        return
    if "|" in output['result']:
        output_type = output['result'].split("|")
    else:
        output_type = []
        output_type.append(output['result'])
    #TODO: miss index type!! with that we would like to reindex document
    for type in output_type:
        if "mail" in type or "stdout" in type:
            output_message = "IOC description: "+output['ioc_description']+"\n"
            output_message += "Risk: "+str(risk) + "\n"
            output_message += "Total Hits: "+str(total)+"\n"
            output_message += "----------------------------------\n\n"
            raw_output =      "------- RAW MESSAGES -------------\n\n"
            # Max raw messages that should be append
            max_append = 1
            for event in message:
                if "_source" in event:
                    for outputField in output['outputfields'].split(","):
                        if outputField in event['_source']:
                            output_message = output_message + " " + outputField+" = "+str(event['_source'][outputField])
                output_message += "\n"
                if output['include_raw'] and max_append <= 50:
                    raw_output += str(event['_source'][output['include_raw']])+"\n"
                    max_append += 1
                        #print "src ip: "+event['_source']['src_ip']+" dst ip: "+event['_source']['dst_ip']+" service: "+event['_source']['dst_port']
            output_message += raw_output
            if "mail" in type:
                sendMail(output['smtp'],output['subject_header'],output_message,output['to'],output['from'],risk+" "+output['ioc_name'])
            else:
                print output_message
        # TODO: should we consider to force stdout or mail with this output?
        if "index" in type:
            if not output['include_raw']:
                print "Error you should specify elastic index with index options"
                continue
            if not output['type']:
                print "Error you should elastic document type with index options"
                continue
            es_message = {}
            for indexField in output['indexfields'].split(","):
                # we consider only the first element for returned values
                # TODO: should we consider enought ?
                if indexField in message[0]['_source']:
                    if "@timestamp" in indexField:
                        es_message["event_time"] = message[0]['_source'][indexField]
                    else:
                        es_message[indexField] = str(message[0]['_source'][indexField])
            now = datetime.datetime.utcnow()
            es_currenttime = now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"
            es_message["@timestamp"] = es_currenttime
            es_message["risk"] = risk
            es_message["involved_events"] = message
            es_message["SignatureName"] = output['ioc_name']
            es_message["description"] = output['ioc_description']
            es_output = elastic_object.index(index=output['es_index'], doc_type=output['type'], id="1", body=es_message)
            #print es_output
    return

