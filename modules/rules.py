__author__ = 'stefano'
import es_output
import time
from datetime import datetime, timedelta


# return 0 if no results in the query
def es_simple_query(utc_time,track_field,track_value,index,type,frequency,query):
    # TODO: add this in config file
    """

    :param utc_time:
    :param track_field:
    :param track_value:
    :param index:
    :param type:
    :param frequency:
    :param query:
    :return:
    """
    strick_field = False
    date_object = datetime.strptime(utc_time, '%Y-%m-%dT%H:%M:%S.000Z')
    # Time frame = time of event +- frequency
    from_timestamp = (int(time.mktime(date_object.timetuple()))+7200)*1000-int(frequency[:-1])
    to_timestamp = (int(time.mktime(date_object.timetuple()))+7200)*1000+int(frequency[:-1])
    # use file inside query string instead of use it in bool filter
    if strick_field:
        query_track_field = """{ \"query\": {
                \"filtered\": {
                  \"query\": {
                    \"bool\": {
                      \"should\": [
                        {
                          \"query_string\": {
                            \"query\": \""""+query+"""\"
                          }
                        }
                      ]
                    }
                  },
                \"filter\": {
                \"bool\" : {
                  \"should\": [{ \"term\": {
                     \""""+track_field+"""\": \""""+track_value+"""\"}}
                   ],
                  \"must\": [ {
                      \"range\": {
                        \"@timestamp\" : {
                          \"from\":\"""" + str(from_timestamp) + """\",
                          \"to\":\"""" + str(to_timestamp) + """\" }}}]}}}}}"""
    else:
         query_track_field = """{ \"query\": {
            \"filtered\": {
              \"query\": {
                \"bool\": {
                  \"should\": [
                    {
                      \"query_string\": {
                        \"query\": \""""+track_value +" OR "+query+"""\"
                      }
                    }
                  ]
                }
              },
            \"filter\": {
            \"bool\" : {
              \"must\": [ {
                  \"range\": {
                    \"@timestamp\" : {
                      \"from\":\"""" + str(from_timestamp) + """\",
                      \"to\":\"""" + str(to_timestamp) + """\" }}}]}}}}}"""
    try:
        res = elastic.search(
            index=[index +  time.strftime("%Y.%m.%d", time.gmtime())],
            doc_type=type,
            body=query_track_field)
        print res
        print query_track_field
    except Exception as inst:
        print inst
        exit()
    if int(res['hits']['total']) > 0:
        return res

    return 0


# gt , lt , equal
def total_action(operand,data,value):
    """

    :param operand: gt, lt, equal
    :param data: array of query results that contains the total hits
    :param value: value to compare taken from RULES of IOC's config file
    :return:
    """
    if "gt" in operand:
        if int(data['hits']['total']) >= int(value):
            return True
    if "lt" in operand:
        if int(data['hits']['total']) <= int(value):
            return True
    if "equal" in operand:
        if int(value) == int(data['hits']['total']):
            return True

    return False


# higher rule match first (rules are evaluated in the order where are written for list
# or simple query on the first match IOC end! )
def rule_parsing(rules,output,data,options,elastic_object):
    """

    :param rules: list of rules taken from IOC config file
    :param output: list fo output options taken from IOC config file
    :param data: array of ['hits']['hits'] that contains the results of query
    :param options: options.[] taken from scripts execution
    :param elastic_object: elasticsearch instance/object so we can run query if needed
    :return:
    """
    global elastic
    # Special handling for sum_query_chain so we output single alarm after all rules
    searchall = False
    # this is elasticsearch object taken from main execution here we can run query
    elastic = elastic_object
    for key,rule in rules.iteritems():
        if options.debug:
            print "Processing Rule: "+rule
        if "foreach" in rule:
            # Take the search operands
            operands_raw = rule.split("\"")
            operands = operands_raw[0].split(" ")
        else:
            operands = rule.split(" ")
        if "total" in operands[0]:
            if total_action(operands[1],data,operands[2]):
                es_output.process_output(output,data['hits']['hits'],data['hits']['total'],operands[4],elastic_object)
                return
        if "foreach" in operands[0]:
            if "search" in operands[2]:
                es_configuration = operands_raw[1].split(",")
                if operands[1] in data['_source'].keys():
                    # track field, index, type, query
                    # results is the output of the query
                    results = es_simple_query(data['_source']['@timestamp'],operands[1],data['_source'][operands[1]],es_configuration[0],es_configuration[1],es_configuration[2],es_configuration[3])
                    if results:
                        operand_total = operands_raw[2].split(" ")
                        # operand(gt,lt,equal), data, operand value
                        if total_action(operand_total[1],results['hits']['hits'],operand_total[2]):
                            # Special handle for rule-type: sum_query_chain
                            if "searchall" in operands[2]:
                                if not "risk" in locals() and not "data_total" in locals():
                                    risk = 0
                                    data_hits_total = []
                                    data_total_total = 0
                                risk += operand_total[4]
                                data_hits_total += data['hits']['hits']
                                data_total_total += data['hits']['total']
                                searchall = True
                            else:
                                es_output.process_output(output,data['hits']['hits'],data['hits']['total'],operand_total[4],elastic_object)

    if searchall:
        es_output.process_output(output,data_hits_total,data_total_total,risk)
    return


def get_index():
    """


    :return: index date of current day and yesterday exception if hour is from 00 and 02
    """
    if int(time.strftime("%H")) > 03 and int(time.strftime("%H")) <= 23:
        yesterday = datetime.utcfromtimestamp(time.time()) - timedelta(days=1)
        yesterday_index = yesterday.strftime("%Y.%m.%d")
        today_index = time.strftime("%Y.%m.%d", time.gmtime())
        return today_index,yesterday_index
    elif int(time.strftime("%H")) == 23:
        yesterday = datetime.utcfromtimestamp(time.time()) - timedelta(days=1)
        yesterday_index = yesterday.strftime("%Y.%m.%d")
        today_index = time.strftime("%Y.%m.%d", time.gmtime())
        return today_index,yesterday_index
    else:
        yesterday = datetime.utcfromtimestamp(time.time()) - timedelta(days=1)
        yesterday_index = yesterday.strftime("%Y.%m.%d")
        return yesterday_index,yesterday_index
