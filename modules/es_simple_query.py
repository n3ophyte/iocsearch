__author__ = 'stefano'
import rules
import os
import sys

from elasticsearch import Elasticsearch


def simple_query(iocdata,options):
    """

    :param iocdata:
    :param options:
    :return:
    """
    if not iocdata['overwrite_frequency']:
        return False
    if options.debug:
        print "from frequency:" + str(iocdata['overwrite_frequency']['from'])
        print "to frequency: " + str(iocdata['overwrite_frequency']['to'])
    ssl = False
    if iocdata['input']['use_ssl'] == "True":
        ssl = True
    if iocdata['input']['username'] and iocdata['input']['password']:
        # ,use_ssl=True
        es = Elasticsearch(host=iocdata['input']['host'],
                             port=iocdata['input']['port'], http_auth=(iocdata['input']['username'], iocdata['input']['password']),use_ssl=ssl)
    else:
        es = Elasticsearch(host=iocdata['input']['host'], port=iocdata['input']['port'], use_ssl=ssl)

    if not os.path.exists(iocdata['input']['source']):
        if options.debug:
            print 'Query files ' + iocdata['input']['source'] + " not found"
        sys.exit(0)
    else:
        #TODO: found a better way to save query because with this method we have to run another file that contain the query!
        line_hash = open(iocdata['input']['source'], 'r')
        query = ""
        for line in line_hash:
            query += line
    today_index, yesterday_index = rules.get_index()
    query_range = """{ """+query+""",
                    \"filter\": {
                    \"bool\" : {
                      \"must\": [ {
                          \"range\": {
                            \"@timestamp\" : {
                              \"from\":\"""" + str(iocdata['overwrite_frequency']['from']*1000) + """\",
                              \"to\":\"""" + str(iocdata['overwrite_frequency']['to']*1000) + """\" }}}]}}}"""
    try:
        res = es.search(
            index=[iocdata['input']['index_prefix'] +  today_index, iocdata['input']['index_prefix'] + yesterday_index],
            doc_type=iocdata['input']['type'],
            body=query_range)
    except Exception as inst:
        print inst
        sys.exit(0)

    if rules.rule_parsing(iocdata['rules'],iocdata['output'],res, options, es):
        return True

    return False